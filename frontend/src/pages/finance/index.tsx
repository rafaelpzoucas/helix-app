import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import TimelineItem from '../../components/TimelineItem'
import styles from './styles.module.scss'

export default function Finance() {
  return (
    <div className={styles.financePage}>
      <section className={styles.currentInvoice}>
        <span>Fatura atual</span>
        <h1>R$ 79,00</h1>
        <span>
          de
          <strong>
            {' 31/05 '}
          </strong>
          até
          <strong>
            {' 06/06'}
          </strong>
        </span>
        <button>Efetuar pagamento</button>
      </section>

      <section className={styles.timeline}>
        <TimelineItem />
        <TimelineItem />
        <TimelineItem />
        <TimelineItem />
        <TimelineItem />
        <TimelineItem />
        <TimelineItem />
        <TimelineItem />
        <TimelineItem />
        <TimelineItem />
        <TimelineItem />
        <TimelineItem />
        <TimelineItem />
      </section>

    </div>
  )
}
