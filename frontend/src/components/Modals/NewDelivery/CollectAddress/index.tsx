import React from 'react'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'

library.add(fas)

import styles from './styles.module.scss'

export default function CollectAddress({onClose = () => {}}) {

    return(
        <>
            <div className={styles.newDeliveryContainer}>
                <section className={styles.headerContainer}>
                    <div className={styles.backTitle}>
                        <button onClick={onClose}>
                            <FontAwesomeIcon icon="arrow-left" />
                        </button>
                        <h1>Solicitar entregador</h1>
                    </div>

                    {/* <button onClick={onClose}>
                        <FontAwesomeIcon icon="times" />
                    </button> */}
                </section> 

                <section className={styles.prevData}>
                    <div className={styles.title}>
                        <FontAwesomeIcon icon="box-open" />
                        <h3>Informações da entrega</h3>
                    </div>
                    <span>Endereço da entrega</span>
                    <p>Rua Do Endereço Da Entrega, 123 - Assis SP</p>
                    <span>Referência</span>
                    <p>Próximo ao Bar do Zé</p>
                    <span>Forma de pagamento</span>
                    <p>Cartão</p>
                </section>

                <section className={styles.collectAddress}>
                    <div className={styles.title}>
                        <FontAwesomeIcon icon="map-marker-alt" />
                        <h3>Endereço de coleta</h3>
                    </div>

                    <div className={styles.collectAddressCard}>
                        <strong>Apelido</strong>
                        <p>Rua Endereço do Restaurante, 1234</p>
                        <span>Ao lado da Outra Loja</span>
                    </div>

                    <button>+ Adicionar novo endereço</button>
                </section>

                <section className={styles.btnsContainer}>
                    <button className={styles.cancel}>Cancelar</button>
                    <button className={styles.continue} onClick={onClose}>Solicitar entregador</button>
                    {/* <button className={styles.disabled}>Selecione o endereço de coleta</button> */}
                </section>
            </div>
        </>
    )
}