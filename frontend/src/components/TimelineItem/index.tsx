import React, { useState } from 'react'


import Image from 'next/image'

import styles from './styles.module.scss'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'

library.add(fas)

export default function TimelineItem() {
    return (
        <div className={styles.itemTimelineContainer}>
            <FontAwesomeIcon icon="star" />
            <div className={styles.titleDescription}>
                <strong>Bem vindo à</strong>
                <p>Helix!</p>
            </div>
            <span>19 Abr 2021</span>
        </div>
    )
}