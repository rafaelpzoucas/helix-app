import React, { useState } from 'react'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'

library.add(fas)

import Image from 'next/image'

import styles from './styles.module.scss'

export default function DeliveryCard() {
    const [isIssueCardVisible, setIsIssueCardVisible] = useState(false)
    const [isDeliveryCardOpened, setIsDeliveryCardOpened] = useState(true)

    function showHideDeliveryCard() {
        if (isDeliveryCardOpened === true) {
            setIsDeliveryCardOpened(false)
        }else{
            setIsDeliveryCardOpened(true)
        }
    }

    function openIssueCard() {
        setIsIssueCardVisible(true)
    }

    function closeIssueCard() {
        setIsIssueCardVisible(false)
    }

    return(
        
        <div className={styles.deliveryCardContainer}>
            {isIssueCardVisible ? (
                <>
                    <section className={styles.idDateDelivery}>
                        <>
                            <span>
                                <strong>#</strong>
                                123456
                            </span>

                            <span>
                                Hoje - 18:07:32
                            </span>
                        </>

                        <button onClick={closeIssueCard}>
                            <Image 
                                width={16} 
                                height={16} 
                                src="/img/icons/cancel.svg"
                                alt="Entregas"
                            />
                        </button>
                    </section>

                    <section>
                        <article>
                                <Image 
                                    width={30} 
                                    height={30} 
                                    src="/img/icons/warning-orange.svg"
                                    alt="Entregas"
                                />
                                <span>Pedido incompleto</span>
                        </article>

                        <article>
                            <Image 
                                width={30} 
                                height={30} 
                                src="/img/icons/warning-orange.svg"
                                alt="Entregas"
                            />
                            <span>Falta troco ou máquina de cartão</span>
                        </article>

                        <article>
                            <Image 
                                width={30} 
                                height={30} 
                                src="/img/icons/warning-orange.svg"
                                alt="Entregas"
                            />
                            <span>Cliente ausente</span>
                        </article>
                        <article className={styles.issueWithInput}>
                            <div className={styles.titleIssueWithInput}>
                                <Image 
                                    width={30} 
                                    height={30} 
                                    src="/img/icons/warning-orange.svg"
                                    alt="Entregas"
                                />
                                <span>Endereço incorreto</span>
                            </div>
                            <input placeholder="Digite aqui o endereço correto..." />
                        </article>

                        <article className={styles.issueWithInput}>
                            <div className={styles.titleIssueWithInput}>
                                <Image 
                                    width={30} 
                                    height={30} 
                                    src="/img/icons/warning-orange.svg"
                                    alt="Entregas"
                                />
                                <span>Outro</span>
                            </div>
                            <input placeholder="Digite em poucas palavras o ocorrido..." />
                        </article>
                    </section>

                    <section className={styles.correctingIssue}>
                        <div className={styles.titleDelivery}>
                            <Image 
                                width={30} 
                                height={30} 
                                src="/img/icons/check-orange.svg"
                                alt="Entregas"
                            />
                            <strong>Correção</strong>
                        </div>
                        <article>
                            <Image 
                                width={30} 
                                height={30} 
                                src="/img/icons/warning-orange.svg"
                                alt="Entregas"
                            />
                            <span>Preciso que o entregador retorne.</span>
                        </article>

                        <article>
                            <Image 
                                width={30} 
                                height={30} 
                                src="/img/icons/warning-orange.svg"
                                alt="Entregas"
                            />
                            <span>Não preciso que o entregador retorne.</span>
                        </article>
                    </section>

                    <section className={`${styles.columnDirection}`}>
                        <button className={`${styles.disabled} ${styles.lg}`}>
                            Selecione uma das opções para continuar.
                        </button>
                        {/* <button className={`${styles.success} ${styles.xl}`}>Confirmar correção</button> */}
                    </section>
                </>

            ) : (
                
                <>
                    <section className={styles.headerDeliveryCard}>
                        <div  className={styles.idDateDelivery}>
                            <span>
                                Hoje - 18:07:32
                            </span>
                            <span>
                                <strong>#</strong>
                                123456
                            </span>
                        </div>
                        <div className={styles.arrowShowHide}>
                            <button onClick={showHideDeliveryCard}>
                                <FontAwesomeIcon 
                                    icon={
                                        isDeliveryCardOpened ? (
                                            'angle-up'
                                        ) : (
                                            'angle-down'
                                            )
                                        } 
                                        className={styles.faIcon} 
                                    />
                            </button>
                        </div>
                    </section>

                    <section className={styles.deliverymanDelivery} style={isDeliveryCardOpened ? {display: ''} : {display: 'none'}}>
                        <div className={styles.titleDelivery}>
                            <Image 
                                width={16} 
                                height={16} 
                                src="/img/icons/scooter-orange.svg"
                                alt="Entregas"
                            />
                            <strong>Entregador</strong>
                        </div>
                        <a href="#">
                            <div className={styles.profileDeliveryman}>
                                <div className={styles.infoDeliveryman}>
                                    <Image 
                                        width={32} 
                                        height={32} 
                                        src="/img/icons/profile-user.svg"
                                        alt="Entregas"
                                    />
                                    <div className={styles.ratingDeliveryman}>
                                        <span>Nome Do Entregador</span>
                                        <div className={styles.currentRateDeliveryman}>
                                            <Image 
                                                width={16}
                                                height={16}
                                                src="/img/icons/star.svg"
                                                alt="Star"
                                            />
                                            <span>4.8</span>
                                        </div>
                                    </div>
                                </div>
                                <button>
                                    <Image 
                                        width={16} 
                                        height={16} 
                                        src="/img/icons/arrow-right.svg"
                                        alt="Entregas"
                                    />
                                </button>
                            </div>
                        </a>
                    </section>

                    <section className={styles.statusDelivery}>
                        <div className={styles.titleDelivery}>
                            <FontAwesomeIcon icon="circle-notch" />

                            <strong>Status</strong>
                        </div>

                        <span>O entregador está retornando para corrigir o imprevisto...</span>

                        <div className={styles.statusBar}>
                            <div className={styles.waitingDeliverymanStatus}>
                                <div className={styles.waitingDeliverymanCurrentStatus}></div>
                            </div>

                            <div className={styles.deliveryInProgressStatus}>
                                <div className={styles.deliveryInProgressCurrentStatus}></div>
                            </div>
                        </div>

                        <span>30 minutos e 57 segundos</span>
                    </section>

                    <section className={styles.addressContainer} style={isDeliveryCardOpened ? {display: ''} : {display: 'none'}}>
                        <div className={styles.titleDelivery}>
                            <Image 
                                width={16}
                                height={16}
                                src="/img/icons/location-pin.svg"
                                alt="Star"
                            />
                            <strong>Endereço</strong>
                        </div>
                        <div className={styles.addressDelivery}>
                            <span>Rua Do Endereço Da Entrega, 12345</span>
                            <strong>Referência: </strong>
                            <span>Próximo ao Bar do Zé, casa amarela de portão azul</span>
                        </div>
                    </section>

                    <section className={styles.paymentDelivery} style={isDeliveryCardOpened ? {display: ''} : {display: 'none'}}>
                        <div className={styles.titleDelivery}>
                            <Image 
                                width={16}
                                height={16}
                                src="/img/icons/money-orange.svg"
                                alt="Star"
                            />
                            <strong>Pagamento</strong>
                        </div>
                        <span>Pedido pago online.</span>
                    </section>

                    <section className={styles.issueDelivery} style={isDeliveryCardOpened ? {display: ''} : {display: 'none'}}>
                        <div className={styles.titleDelivery}>
                            <Image 
                                width={16}
                                height={16}
                                src="/img/icons/warning-orange.svg"
                                alt="Star"
                            />
                            <strong>Imprevisto</strong>
                        </div>
                        <span>Faltou item do pedido.</span>
                    </section>

                    <section className={styles.btnsDelivery}>
                        <button className={`${styles.warning} ${styles.lg}`} onClick={openIssueCard}>Imprevisto</button>
                        <button className={`${styles.success} ${styles.xl}`}>Finalizar</button>
                    </section>
                    
                </>
            )}
        </div>
    )
}