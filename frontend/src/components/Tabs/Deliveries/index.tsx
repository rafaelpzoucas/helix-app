import React from 'react'
import styles from './styles.module.scss'

import DeliveryCard from '../../DeliveryCard'

export default function Home() {
  return (
    <div className={styles.deliveriesPage}>
      <main>
        <div className={styles.groupBtn}>
          <button  className={styles.groupBtnActive}>
              Em espera
          </button>
          <button>
              Em andamento
          </button>
          <button>
              Finalizadas
          </button>
        </div>
        
        <div className={styles.deliveriesContainer}>
          {/* <section style={{display: `${onHoldDeliveries}`}}>
            <DeliveryCard status="onHold" />
            <DeliveryCard status="onHold" />
            <DeliveryCard status="onHold" />
          </section>
          <section style={{display: `${inProgressDeliveries}`}}>
            <DeliveryCard status="inProgress" />
            <DeliveryCard status="inProgress" />
            <DeliveryCard status="inProgress" />
          </section>
          <section style={{display: `${finishedDeliveries}`}}>
            <DeliveryCard status="finished" />
            <DeliveryCard status="finished" />
            <DeliveryCard status="finished" />
          </section> */}

          <DeliveryCard />
          <DeliveryCard />
          <DeliveryCard />
          <DeliveryCard />
        </div>

        {/* ------- ativar quando não houver entregas -------- */}
        {/* <div className={styles.theresNoDelivery}>
          Não há entregas em espera
        </div> */}
      </main>
    </div>
  )
}
