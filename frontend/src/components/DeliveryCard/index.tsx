import React, { useState } from 'react'
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock'

import Image from 'next/image'

import styles from './styles.module.scss'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import IssueCard from '../Modals/Issue'
import Issue from '../Modals/Issue'

library.add(fas)

export default function DeliveryCard() {
    const [isIssueCardVisible, setIsIssueCardVisible] = useState(false)
    const [isDeliveryCardOpened, setIsDeliveryCardOpened] = useState(true)
    const [isModalIssueVisible, setIsMotalIssueVisible] = useState(false)
    const [displayDeliveryCardItems, setDisplayDeliveryCardItems] = useState('none')

    function showHideDeliveryCard() {
        if (isDeliveryCardOpened === true) {
            setIsDeliveryCardOpened(false)
            setDisplayDeliveryCardItems('')
        }else{
            setIsDeliveryCardOpened(true)
            setDisplayDeliveryCardItems('none')
        }
    }

    const openModalIssue = () => {
        setIsMotalIssueVisible(true)
        disableBodyScroll(document)
    }  

    return (
        <div className={styles.deliveryCardContainer}>
            <section id="header" className={styles.headerContainer} onClick={showHideDeliveryCard}>
                <div className={styles.dateTimeId}>
                    <span className={styles.dateTime}>Hoje - 14:53</span>

                    <span id="idDelivery" className={`${styles.titleDelivery} ${styles.idDelivery}`}>
                        <FontAwesomeIcon icon="hashtag" />
                        <strong>1973</strong>
                    </span>

                </div>

                {isDeliveryCardOpened ? (
                    <FontAwesomeIcon 
                        icon="chevron-down" 
                        className={styles.showHideDeliveryCard}
                    />
                ) : (
                    <FontAwesomeIcon 
                        icon="chevron-up" 
                        className={styles.showHideDeliveryCard}
                    />
                )}
            </section>

            <section id="status" className={styles.statusContainer}>
                <h3 className={styles.titleDelivery}>
                    <FontAwesomeIcon icon="circle-notch" />
                    <strong>Status da entrega</strong>
                </h3>

                <div className={styles.statusBar}>
                    <div className={styles.waitingDeliverymanStatus}>
                        <div className={styles.waitingDeliverymanCurrentStatus}></div>
                    </div>

                    <div className={styles.deliveryInProgressStatus}>
                        <div className={styles.deliveryInProgressCurrentStatus}></div>
                    </div>
                </div>

                <p>O entregador está retornando para corrigir o imprevisto...</p>

                <span>30 minutos e 57 segundos</span>
            </section>

            <section id="deliveryMan" className={styles.deliverymanContainer} style={{display: `${displayDeliveryCardItems}`}}>
                <h3 className={styles.titleDelivery}>
                    <FontAwesomeIcon icon="motorcycle" />
                    <strong>Informações do entregador</strong>
                </h3>

                <div className={styles.profileLink}>
                    <div className={styles.info}>
                        <Image 
                            width={32} 
                            height={32} 
                            src="/img/icons/profile-user.svg"
                            alt="Profile"
                        />
                        <div className={styles.deliveryman}>
                            <span>
                                Nome do Entregador
                            </span>

                            <div className={styles.currentRate}>
                                <FontAwesomeIcon icon="star" />
                                <span>4.8</span>
                            </div>
                        </div>
                    </div>

                    <FontAwesomeIcon icon="angle-right" className={styles.arrow} />
                </div>
            </section>

            <section id="address" className={styles.addressContainer} style={{display: `${displayDeliveryCardItems}`}}>
                <h3 className={styles.titleDelivery}>
                    <FontAwesomeIcon icon="map-marker-alt" />
                    <strong>Endereço da entrega</strong>
                </h3>
                    <p>Rua Do Endereço Da Entrega, 345</p>

                <span>Referência:</span>

                <p>Próximo ao Bar Do Zé, Casa amarela de portão azul.</p>
            </section>

            <section id="payment" style={{display: `${displayDeliveryCardItems}`}}>
                <h3 className={styles.titleDelivery}>
                    <FontAwesomeIcon icon="exclamation-triangle" />
                    <strong>Imprevisto</strong>
                </h3>
                <p>Faltou um item do pedido.</p>
            </section>

            <section id="payment" style={{display: `${displayDeliveryCardItems}`}}>
                <h3 className={styles.titleDelivery}>
                    <FontAwesomeIcon icon="coins" />
                    <strong>Forma de pagamento</strong>
                </h3>
                <p>Cartão</p>
            </section>

            <div id="actionButtons" className={styles.actionButtonsContainer}>
                <button className={styles.cancelBtn}>Cancelar</button>

                <div className={styles.progressBtns}>
                    <button onClick={openModalIssue} className={styles.issueBtn}>Imprevisto</button>

                    <button className={styles.finalizeBtn}>Finalizar</button>
                </div>
            </div>

            {isModalIssueVisible ? (
              <Issue 
                onClose={() => {
                    setIsMotalIssueVisible(false)
                    enableBodyScroll(document)
                }} 
              />
            ) : null}
        </div>
    )
}