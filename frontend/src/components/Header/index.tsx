import React from 'react'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'

library.add(fas)


import styles from './styles.module.scss'
import { useState } from 'react'
import Notifications from '../Modals/Notifications'

export default function Header(props) {
    const [isNotificationsVisible, setIsNotificationsVisible] = useState(false)

    const openNotifications = () => {
        setIsNotificationsVisible(true)
    }
    
    return(
        <>
            <div className={styles.headerContainer}>
                <h1>{props.title}</h1>

                <div className={styles.buttonsHeader}>
                    <button>
                        <FontAwesomeIcon icon="search" className={styles.faIcon} />
                    </button>

                    <button onClick={openNotifications}>
                        <FontAwesomeIcon icon="bell"  className={styles.faIcon} />
                    </button>
                </div>
            </div>
            
            {isNotificationsVisible ? <Notifications onClose={() => setIsNotificationsVisible(false)} /> : null}
        </>
    )
}