import React, { useState } from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import Link from 'next/link'

import styles from './styles.module.scss'
import NewDelivery from '../Modals/NewDelivery'

export default function Sidebar() {
    const [isModalVisible, setIsModalVisible] = useState(false)

    const openModal = () => {
        setIsModalVisible(true)
    }
    
    return(
        <>
            <div className={styles.navbarContainer}>
                <div className={styles.navbarBtn}>
                    <FontAwesomeIcon icon="bars" />
                </div>
                <div className={`${styles.navbarBtn}`}>
                    <Link href="/">
                        <FontAwesomeIcon icon="motorcycle" />
                    </Link>
                </div>
                <div className={styles.navbarBtn}>
                    <Link href="/history">
                        <FontAwesomeIcon icon="history" />
                    </Link>
                </div>
                <div className={styles.navbarBtn}>
                    <Link href="/finance">
                        <FontAwesomeIcon icon="coins" />
                    </Link>
                </div>
                <button onClick={openModal} className={styles.newDelivery}>
                    <span>+</span>
                </button>
            </div>
            {isModalVisible ? <NewDelivery onClose={() => setIsModalVisible(false)} /> : null}
        </>
    )
}